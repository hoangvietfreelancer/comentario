module gitlab.com/comentario/comentario

go 1.22

require (
	github.com/JohannesKaufmann/html-to-markdown v1.5.0
	github.com/avct/uasurfer v0.0.0-20240501094946-ca0c4d1e541b
	github.com/disintegration/imaging v1.6.2
	github.com/doug-martin/goqu/v9 v9.19.0
	github.com/go-openapi/errors v0.22.0
	github.com/go-openapi/loads v0.22.0
	github.com/go-openapi/runtime v0.28.0
	github.com/go-openapi/spec v0.21.0
	github.com/go-openapi/strfmt v0.23.0
	github.com/go-openapi/swag v0.23.0
	github.com/go-openapi/validate v0.24.0
	github.com/google/uuid v1.6.0
	github.com/gorilla/csrf v1.7.2
	github.com/gorilla/handlers v1.5.2
	github.com/gorilla/websocket v1.5.1
	github.com/hashicorp/go-version v1.6.0
	github.com/jellydator/ttlcache/v3 v3.2.0
	github.com/jessevdk/go-flags v1.5.0
	github.com/justinas/alice v1.2.0
	github.com/lib/pq v1.10.9
	github.com/markbates/goth v1.79.0
	github.com/mattn/go-sqlite3 v1.14.22
	github.com/microcosm-cc/bluemonday v1.0.26
	github.com/nicksnyder/go-i18n/v2 v2.4.0
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/phuslu/iploc v1.0.20240501
	github.com/yuin/goldmark v1.7.1
	golang.org/x/crypto v0.23.0
	golang.org/x/net v0.25.0
	golang.org/x/text v0.15.0
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	gopkg.in/yaml.v3 v3.0.1
)

require (
	cloud.google.com/go/compute/metadata v0.3.0 // indirect
	github.com/PuerkitoBio/goquery v1.9.2 // indirect
	github.com/andybalholm/cascadia v1.3.2 // indirect
	github.com/asaskevich/govalidator v0.0.0-20230301143203-a9d515a09cc2 // indirect
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/docker/go-units v0.5.0 // indirect
	github.com/felixge/httpsnoop v1.0.4 // indirect
	github.com/go-openapi/analysis v0.23.0 // indirect
	github.com/go-openapi/jsonpointer v0.21.0 // indirect
	github.com/go-openapi/jsonreference v0.21.0 // indirect
	github.com/google/go-cmp v0.6.0 // indirect
	github.com/gorilla/css v1.0.1 // indirect
	github.com/gorilla/securecookie v1.1.2 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/mrjones/oauth v0.0.0-20190623134757-126b35219450 // indirect
	github.com/oklog/ulid v1.3.1 // indirect
	go.mongodb.org/mongo-driver v1.15.0 // indirect
	golang.org/x/image v0.16.0 // indirect
	golang.org/x/oauth2 v0.20.0 // indirect
	golang.org/x/sync v0.7.0 // indirect
	golang.org/x/sys v0.20.0 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
)
