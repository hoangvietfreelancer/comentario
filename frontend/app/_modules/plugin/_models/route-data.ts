import { PluginConfig, PluginUIPlugConfig } from '../../../../generated-api';

export interface RouteData {
    plugin: PluginConfig;
    plug:   PluginUIPlugConfig;
}
